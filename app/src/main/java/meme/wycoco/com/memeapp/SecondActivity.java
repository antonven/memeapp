package meme.wycoco.com.memeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        TextView txtTopDisplay = (TextView)findViewById(R.id.txtTopDisplay);
        TextView txtBotDisplay = (TextView)findViewById(R.id.txtBotDisplay);

        assert txtTopDisplay != null;
        txtTopDisplay.setText(getIntent().getStringExtra("top"));
        assert txtBotDisplay != null;
        txtBotDisplay.setText(getIntent().getStringExtra("bot"));

    }
}
