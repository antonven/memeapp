package meme.wycoco.com.memeapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editTop = (EditText)findViewById(R.id.editTop);
        final EditText editBot = (EditText)findViewById(R.id.editBot);
        Button  btnSend = (Button)findViewById(R.id.btnSend);

        assert btnSend != null;
        btnSend.setOnClickListener(
                new Button.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        String top = editTop.getText().toString();
                        String bot = editBot.getText().toString();
                        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtra("top", top);
                        intent.putExtra("bot", bot);
                        startActivity(intent);

                    }
                }
        );

    }
}
